# README #

Igor was an Integrated Test Environment (ITE) for the defunct Frank project which was an iOS acceptance test framework.
Igor allowed you to easily:

* Write & Run acceptance tests
* See and insert all your Frank steps into your tests
* Create a Frankified build, or run a custom build script
 
![Alt text](https://bytebucket.org/sbrost/igor/raw/4d67ee240ae11f4d1f06f854eb1b8c5f067eb262/ScreenShot.png)