//
//  TerminalView.m
//  Igor
//
//  Created by Steve Brost on 8/11/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import "TerminalView.h"
#import "iTerm/iTerm.h"
#import "PTYSession.h"
#import "PTYTask.h"
#import "PSMTabBarControl.h"

@interface TerminalView (Private)

- (void)goToDirectory:(NSString*)path;
- (NSString*)projetFolderForFeatureAtPath:(NSString*)path;
- (void)buildFrankAppRelativeToPath:(NSString*)testPathToFeature;

@end

@implementation TerminalView


- (void)awakeFromNib
{
    [iTermController sharedInstance];
	
	NSDictionary* dict = [[ITAddressBookMgr sharedInstance] defaultBookmarkData];
	terminalView = [ITTerminalView view:dict];
    
	[terminalView setFrame:[self bounds]];
    [[terminalView tabBarControl] setHideForSingleTab: YES];

    
    [terminalView setFont: [NSFont fontWithName: @"Menlo" size:11.0] nafont: nil];
    [terminalView addNewSession:dict withCommand:nil withURL:nil];
    
    PTYSession* session = [[terminalView sessions] objectAtIndex: 0];
	[session setForegroundColor: [NSColor whiteColor]];
    [session setBackgroundColor: [NSColor blackColor]];
    [session setSelectionColor: [NSColor lightGrayColor]];
    [session setCursorColor: [NSColor blackColor]];
    
    
	[self addSubview:terminalView];
}


- (void)goToDirectory:(NSString *)path
{
    [terminalView runCommand:[NSString stringWithFormat:@"cd \"%@\"", path]];
}

- (NSString*)projetFolderForFeatureAtPath:(NSString*)path
{
    NSString* featuresFolder = [path stringByDeletingLastPathComponent];
    NSString* frankFolder    = [featuresFolder stringByDeletingLastPathComponent];
    NSString* projectFolder  = [frankFolder stringByDeletingLastPathComponent];
    
    return projectFolder;
}


- (void)buildFrankAppRelativeToPath:(NSString*)testPathToFeature andLaunchIt:(BOOL)launchIt
{
    [self buildFrankAppRelativeToPath:testPathToFeature];
    
    if (launchIt)
        [terminalView runCommand:[NSString stringWithFormat:@"frank launch"]]; //assuming we are alredy in the correct folder from buildFrankAppRelativeToPath
}

- (void)buildFrankAppRelativeToPath:(NSString*)testPathToFeature
{
    NSString* projectFolder  = [self projetFolderForFeatureAtPath:testPathToFeature];
    [self goToDirectory:projectFolder];
    
    NSString* buildCommand = nil;
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* buildPref = [defaults objectForKey:kBuildCommandKey];
    if ([buildPref isEqualToString:kBuildCommandFrankBuild])
        buildCommand = @"frank build";
    else if ([buildPref isEqualToString:kBuildCommandIgorBuild])
        buildCommand = @"./igor_build";
    else //no build pref ... uho-oh
        buildCommand = @"echo \"no build command\"";

    [terminalView runCommand:buildCommand];
}

- (void)runFeatureAtPath:(NSString*)testPathToFeature
{
    [terminalView runCommand: [NSString stringWithFormat: @"cucumber \"%@\"", testPathToFeature]];
}

- (void)stopFeatureAtPath:(NSString*)testPathToFeature
{
    [terminalView stopRunningCommand];
}

- (void)clear:(id)sender
{
    [terminalView clearBuffer: sender];
}

@end
