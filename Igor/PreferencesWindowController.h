//
//  PreferencesWindowController.h
//  Igor
//
//  Created by Steve Brost on 8/18/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface PreferencesWindowController : NSWindowController <NSWindowDelegate>
{
}

@property (nonatomic, assign) IBOutlet NSTextView* defaultFeatureTextView;
@property (nonatomic, assign) IBOutlet NSMatrix*   buildMatrix;

@end
