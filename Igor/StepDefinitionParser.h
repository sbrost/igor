//
//  StepDefinitionParser.h
//  Igor
//
//  Created by Steve Brost on 5/1/12.
//  Copyright (c) 2012 Software Pot Pie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StepDefinitionParser : NSObject
{
    NSMutableArray* definitions;
    NSURL* definitionsDirectory;
}

- (id)initWithDirectory:(NSURL*)directoryURL;

- (void)reload;

@property (nonatomic, readonly) NSArray* definitions;

@end
