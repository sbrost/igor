//
//  PreferencesWindowController.m
//  Igor
//
//  Created by Steve Brost on 8/18/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import "PreferencesWindowController.h"


@implementation PreferencesWindowController

- (id) init
{
	if ( (self = [super initWithWindowNibName: @"Prefs"]) ) 
	{
		
	}
	return self;
}

- (void)dealloc
{
    self.defaultFeatureTextView = nil;
    [super dealloc];
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    [[self window] setDelegate: self];
    [[self window] center];

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* defaultText = [defaults stringForKey: kDefaultFeatureTextKey];
    if (defaultText != nil)
        [_defaultFeatureTextView setString: defaultText];
    [_defaultFeatureTextView setFont: [NSFont fontWithName: @"Menlo" size: 11.0]];
    
    const NSInteger kFrankBuildTag = 1;
    const NSInteger kIgorBuildTag  = 2;
    NSString* buildCommand = [defaults objectForKey:kBuildCommandKey];
    if ([buildCommand isEqualToString:kBuildCommandFrankBuild])
        [_buildMatrix selectCellWithTag:kFrankBuildTag];
    else if ([buildCommand isEqualToString:kBuildCommandIgorBuild])
        [_buildMatrix selectCellWithTag:kIgorBuildTag];
}

- (void)windowWillClose:(NSNotification*)notification;
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: [_defaultFeatureTextView string] forKey: kDefaultFeatureTextKey];
}

- (IBAction)frankBuild:(id)sender
{
     NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:kBuildCommandFrankBuild forKey:kBuildCommandKey];
}

- (IBAction)igorBuild:(id)sender
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:kBuildCommandIgorBuild forKey:kBuildCommandKey];
}


@end
