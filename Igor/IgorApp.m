//
//  IgorApp.m
//  Igor
//
//  Created by Steve Brost on 8/18/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import "IgorApp.h"


@implementation IgorApp

- (id)init
{
    self = [super init];
    if (self) 
    {
        NSString* defaultFeatureText = [NSString stringWithFormat: @"Feature: %@\n\nBackground:\n\t%@\n\nScenario: %@\n\t%@", 
                                        @"<Feature description - you can change this default in the preferences>",
                                        @"<Background/Setup steps - you can change this default in the preferences>",
                                        @"<Scenario Name>",
                                        @"<Scenario steps>"];
        
        NSString* defaultCoreFrankSteps = @"/Library/Ruby/Gems/1.8/gems/frank-cucumber-0.6.1/lib/frank-cucumber";
        NSDictionary* appDefaults = @{kDefaultFeatureTextKey: defaultFeatureText,
                                      kFrankCoreStepsDir: defaultCoreFrankSteps,
                                      kBuildCommandKey: kBuildCommandFrankBuild};
        
        [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
     }
    
    return self;
}


@end
