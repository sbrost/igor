//
//  main.m
//  Igor
//
//  Created by Steve Brost on 7/9/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
