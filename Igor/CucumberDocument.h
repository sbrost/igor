//
//  Cucumber_Feature.h
//  Igor
//
//  Created by Steve Brost on 7/9/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class NoodleLineNumberView;
@class TerminalView;
@class DefinitionsVC;
@interface CucumberDocument : NSDocument
{
    IBOutlet NSTextView* codeView;
    IBOutlet NSScrollView* codeScrollView;
    NoodleLineNumberView* lineNumberView;

    IBOutlet TerminalView* terminalView;
    IBOutlet DefinitionsVC* stepDefVC;
    
}

- (IBAction)newScenario:(id)sender;
- (IBAction)buildAndRunFeature:(id)sender;
- (IBAction)buildAndLaunch:(id)sender;
- (IBAction)runFeature:(id)sender;
- (IBAction)comment:(id)sender;
- (IBAction)stopFeature:(id)sender;

- (IBAction)clear:(id)sender;

@property (nonatomic, retain) NSTextView* codeView;
@property (nonatomic, retain) NSScrollView* codeScrollView;
@property (nonatomic, retain) TerminalView* terminalView;

@end


extern NSString* FeatureWithValues(NSString* description, NSString* background, NSString* firstScenarioName);