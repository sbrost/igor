//
//  DefinitionsVC.m
//  Igor
//
//  Created by Steve Brost on 5/1/12.
//  Copyright (c) 2012 Software Pot Pie. All rights reserved.
//

#import "DefinitionsVC.h"
#import "StepDefinitionParser.h"
#import "CucumberDocument.h"

@interface DefinitionsVC ()

@end

@implementation DefinitionsVC
@synthesize definitionsTableView, searchField, blankDocumentWarningLabel;

/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}*/

- (void)dealloc
{
    [parser release];
    [filteredDefinitions release];
    self.definitionsTableView = nil;
    self.searchField = nil;
    self.blankDocumentWarningLabel = nil;
    [super dealloc];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self reload];
    [blankDocumentWarningLabel setHidden: [filteredDefinitions count] != 0 ];
    
    [self.definitionsTableView setTarget:self];
    [self.definitionsTableView setDoubleAction:@selector(doubleClickDefinition:)];
    
}

- (void)reload
{
    NSURL* fileURL = [document fileURL];
    if (fileURL == nil)
        return;
    
    NSString* fileParentDir = [[fileURL path] stringByDeletingLastPathComponent];
    
    NSString* path = [fileParentDir stringByAppendingFormat: @"/step_definitions/"];
    NSURL* definitionsDirectory = [NSURL fileURLWithPath: path isDirectory: YES];
    parser = [[StepDefinitionParser alloc] initWithDirectory: definitionsDirectory];
    filteredDefinitions = [[NSMutableArray alloc] initWithArray: parser.definitions];
    [self search:nil];
    
    [blankDocumentWarningLabel setHidden: [filteredDefinitions count] != 0 ];
    [self.definitionsTableView reloadData];
}

- (NSInteger)numberOfRowsInTableView:(NSTableView*)tableView
{
    return [filteredDefinitions count];
}


- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    return [filteredDefinitions objectAtIndex:row];
}

- (void)doubleClickDefinition:(id)sender
{
    NSValue* selectionRangeValue = [[document.codeView selectedRanges] objectAtIndex: 0];
    NSRange selection = [selectionRangeValue rangeValue];
    
    NSString* stepText = [filteredDefinitions objectAtIndex:[self.definitionsTableView selectedRow]];

    [document.codeView insertText:[NSString stringWithFormat:@"\n\t%@\n", stepText] replacementRange:selection];
}

- (void)search:(id)sender
{
    NSIndexSet* matchingDefinitionIndexes = [parser.definitions indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) 
    {
        NSString* filterTerm = [searchField stringValue];
        if ([filterTerm length] == 0)
            return YES;
        
        NSString* definition = [parser.definitions objectAtIndex:idx];
        if ([definition rangeOfString:filterTerm options:NSCaseInsensitiveSearch].location != NSNotFound)
            return YES;
        
        return NO;
    }];
    
    [filteredDefinitions setArray: [parser.definitions objectsAtIndexes: matchingDefinitionIndexes]];
    [self.definitionsTableView reloadData];
}

@end
