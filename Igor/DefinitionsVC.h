//
//  DefinitionsVC.h
//  Igor
//
//  Created by Steve Brost on 5/1/12.
//  Copyright (c) 2012 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class StepDefinitionParser;
@class CucumberDocument;
@interface DefinitionsVC : NSViewController
{
    StepDefinitionParser*   parser;
    NSMutableArray*         filteredDefinitions;
    
    NSTableView*    definitionsTableView;
    NSSearchField*  searchField;
    NSTextField*    blankDocumentWarningLabel;
    
    IBOutlet CucumberDocument* document;
}

- (IBAction)search:(id)sender;
- (void)reload;

@property (nonatomic, retain) IBOutlet NSTableView* definitionsTableView;
@property (nonatomic, retain) IBOutlet NSSearchField* searchField;
@property (nonatomic, retain) IBOutlet NSTextField* blankDocumentWarningLabel;

@end
