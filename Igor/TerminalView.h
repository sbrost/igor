//
//  TerminalView.h
//  Igor
//
//  Created by Steve Brost on 8/11/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ITTerminalView;
@interface TerminalView : NSView 
{
@private
    ITTerminalView* terminalView;
}

- (void)runFeatureAtPath:(NSString*)testPathToFeature;
- (void)stopFeatureAtPath:(NSString*)testPathToFeature;
- (void)buildFrankAppRelativeToPath:(NSString*)testPathToFeature andLaunchIt:(BOOL)launchIt;

- (IBAction)clear:(id)sender;


@end
