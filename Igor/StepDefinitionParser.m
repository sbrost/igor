//
//  StepDefinitionParser.m
//  Igor
//
//  Created by Steve Brost on 5/1/12.
//  Copyright (c) 2012 Software Pot Pie. All rights reserved.
//

#import "StepDefinitionParser.h"

NSString* kRubyExtension = @"rb";

@interface StepDefinitionParser (Private)

- (void)parseFileContents:(NSString*)fileContents;

@end

@implementation StepDefinitionParser
@synthesize definitions;

- (id)initWithDirectory:(NSURL*)directoryURL
{
    if ( (self = [super init]) )
    {
        definitionsDirectory = [directoryURL retain];
        definitions = [[NSMutableArray alloc] init];
        [self reload];
        
    }
    return self;
}

- (void)dealloc
{
    [definitions release];
    [definitionsDirectory release];
    [super dealloc];
}

- (void)appendDefaultFrankStepsTo:(NSMutableArray*)contents
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSURL* coreFrankSteps = [NSURL fileURLWithPath:[defaults stringForKey:kFrankCoreStepsDir] isDirectory:YES];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    NSError* error;
    NSArray* coreFrankStepsfiles = [fileManager contentsOfDirectoryAtURL: coreFrankSteps includingPropertiesForKeys: [NSArray arrayWithObjects:NSURLNameKey, nil] options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    
    [contents addObjectsFromArray:coreFrankStepsfiles];
}

- (void)reload
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSError* error;
    NSMutableArray* contents = [NSMutableArray arrayWithArray:[fileManager contentsOfDirectoryAtURL: definitionsDirectory includingPropertiesForKeys: [NSArray arrayWithObjects:NSURLNameKey, nil] options:NSDirectoryEnumerationSkipsHiddenFiles error:&error]];
    
    [self appendDefaultFrankStepsTo:contents];

    for (NSURL* contentURL in contents)
    {
        NSFileWrapper* fileWrapper = [[NSFileWrapper alloc] initWithURL: contentURL options: NSFileWrapperReadingWithoutMapping error:&error];
        if (![fileWrapper isDirectory])
        {
            NSString* fileExtension = [[fileWrapper filename] pathExtension];
            if ([fileExtension isEqualToString: kRubyExtension])
            {
                NSString* fileContents = [[NSString alloc] initWithContentsOfURL: contentURL encoding: NSUTF8StringEncoding error: &error];
                [self parseFileContents: fileContents];
                [fileContents release];
            }
        }
        [fileWrapper release];
    }
}

- (void)parseFileContents:(NSString*)fileContents
{
    //Then /^the "([^\"]*)" table view cell should (not )?have a (\w*) accessory$/ do |mark,qualifier,expected_accessory|
    //dumb brute force, cuz I dumb
    NSArray* lines = [fileContents componentsSeparatedByString: @"\n"];
    for (NSString* line in lines)
    {
        NSRange end = [line rangeOfString: @"$/" options: NSBackwardsSearch];
        if (end.location != NSNotFound)
        {
            NSMutableString* humanReadable = [NSMutableString stringWithString:[line substringToIndex: end.location]]; 
            [humanReadable setString:[humanReadable stringByReplacingOccurrencesOfString: @"([^\\\"]*)"  withString: @"NAME"]];
            [humanReadable setString: [humanReadable stringByReplacingOccurrencesOfString:@"/^" withString: @""]];
            
            [humanReadable setString:[humanReadable stringByReplacingOccurrencesOfString:@"([\\d.]+)" withString: @"NUMBER"]];
            [humanReadable setString:[humanReadable stringByReplacingOccurrencesOfString:@"(\\d+)" withString: @"NUMBER"]];
            [humanReadable setString:[humanReadable stringByReplacingOccurrencesOfString:@"(\\d*)(?:st|nd|rd|th)?" withString: @"1st, 2nd, 3rd, 4th etc."]];
            
            [humanReadable setString: [humanReadable stringByReplacingOccurrencesOfString: @"(?:s)?" withString:@"(s)"]];
            
            [definitions addObject: humanReadable];
        }
    }
}

@end
