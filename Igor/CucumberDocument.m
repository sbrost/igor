//
//  Cucumber_Feature.m
//  Igor
//
//  Created by Steve Brost on 7/9/11.
//  Copyright 2011 Software Pot Pie. All rights reserved.
//

#import "CucumberDocument.h"
#import "NoodleLineNumberView.h"
#import "TerminalView.h"
#import "DefinitionsVC.h"

NSString* kScenarioTemplate = @"\n\nScenario: ";

@implementation CucumberDocument
@synthesize codeScrollView, codeView, terminalView;

- (void)dealloc 
{
    self.codeView = nil;
    self.codeScrollView = nil;
    self.terminalView = nil;
    [super dealloc];
}

- (NSString *)windowNibName
{
    return @"CucumberDocument";
}

- (void)loadFeatureFileContents
{
    NSData* data = [NSData dataWithContentsOfURL: [self fileURL]];
    NSString* textData = [[NSString alloc]initWithData: data encoding: NSUTF8StringEncoding];
    [self.codeView setString: textData];
    [textData release];    
}

- (void)loadDefaultFeatureContents
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* defaultFeatuerText = [defaults stringForKey: kDefaultFeatureTextKey];
    if (defaultFeatuerText != nil)
    {
        [self.codeView setString: defaultFeatuerText];
    }
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    
    
    //Line number gutter
    lineNumberView = [[NoodleLineNumberView alloc] initWithScrollView:codeScrollView];
    [codeScrollView setVerticalRulerView:lineNumberView];
    [codeScrollView setHasHorizontalRuler:NO];
    [codeScrollView setHasVerticalRuler:YES];
    [codeScrollView setRulersVisible:YES];
    
    //Base font initializtion
    NSFont* editorFont = [NSFont fontWithName: kTextViewFont size: kTextFontSize];
    [self.codeView setFont: editorFont];
    
    //Turning off text substitutions that Frank does not like
    [self.codeView setAutomaticQuoteSubstitutionEnabled:NO];
    [self.codeView setAutomaticDashSubstitutionEnabled:NO];
        
    NSURL* fileURL = [self fileURL];
    if (fileURL != nil)
        [self loadFeatureFileContents];
    else
        [self loadDefaultFeatureContents];

}

- (void)setFileURL:(NSURL*)url
{
    [super setFileURL:url];
    if (stepDefVC != nil)
        [stepDefVC reload];
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError 
{
    return [[self.codeView string] dataUsingEncoding: NSUTF8StringEncoding];
}

- (BOOL)readFromData:(NSData*)data ofType:(NSString*)typeName error:(NSError**)outError 
{
    return YES;
}

- (IBAction)newScenario:(id)sender 
{
    [self.codeView insertText: kScenarioTemplate];
}

- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem
{
    if ([anItem action] == @selector(runFeature:) && [self fileURL] == nil)
        return NO;
   /*Can't determine right now if a feature is running -- revisit*/
    if ([anItem action] == @selector(stopFeature:))
    {
        if ([self fileURL] == nil)
            return NO;
    }
    if ([anItem action] == @selector(comment:) && [[codeView selectedRanges] count] > 1)
        return NO;
    return [super validateUserInterfaceItem: anItem];
}

- (IBAction)buildAndLaunch:(id)sender
{
    [self.terminalView buildFrankAppRelativeToPath:[[self fileURL] path] andLaunchIt:YES]; //the build may fail ... don't have good error checking here
}

- (IBAction)buildAndRunFeature:(id)sender
{
    [self.terminalView buildFrankAppRelativeToPath:[[self fileURL] path] andLaunchIt:NO]; //the build may fail ... don't have good error checking here
    [self runFeature:sender];
}

- (IBAction)runFeature:(id)sender 
{
    if ([self fileURL] == nil)
        return;
    
    if ([self isDocumentEdited])
        [self saveDocument:nil];
    [self.terminalView runFeatureAtPath: [[self fileURL] path]];
}

- (IBAction)stopFeature:(id)sender
{
    if ([self fileURL] == nil)
        return;
    
    [self.terminalView stopFeatureAtPath:[[self fileURL] path]];
}

- (IBAction)clear:(id)sender
{
    [self.terminalView clear: sender];
}

- (NSPrintOperation *)printOperationWithSettings:(NSDictionary *)printSettings error:(NSError**)outError
{
    return [NSPrintOperation printOperationWithView:self.codeView];
}

- (IBAction)comment:(id)sender
{
    NSValue* selectionRangeValue = [[codeView selectedRanges] objectAtIndex: 0];
    NSRange selection = [selectionRangeValue rangeValue];
    
    NSMutableString* fullFeature = [NSMutableString stringWithString:[codeView string]];
    
    NSMutableArray* lineStarts = [NSMutableArray new];
    
    NSInteger offset = selection.location;
    while (offset >= 0) //line of the selection begin
    {
        unichar character = [fullFeature characterAtIndex:offset];
        if (character == '\n' || character == '\r')
        {
            [lineStarts addObject: [NSNumber numberWithInteger:offset+1]];
            break;
        }
        else if (offset == 0)
        {
            [lineStarts addObject: [NSNumber numberWithInteger:offset]];
            break;
        }
            
        offset--;
            
    }
    offset = selection.location;
    while (offset < selection.location + selection.length) //line of the selection begin
    {
        unichar character = [fullFeature characterAtIndex:offset];
        if (character == '\n' || character == '\r')
            [lineStarts addObject: [NSNumber numberWithInteger:offset+1]];
        offset++;
    }
    
    
    NSInteger shift = 0;
    for (NSNumber* lineStart in lineStarts)
    {
        NSInteger lineOffset = [lineStart integerValue];
        
        lineOffset += shift;

        
        unichar characterAtOffset = [[codeView string] characterAtIndex:lineOffset];
       /* NSString* cString = [NSString stringWithCharacters:&characterAtOffset length:1];
        NSLog(@"%@", cString);*/
        
        NSInteger shiftValue = 1;
        NSInteger insertLength = 0;
        NSMutableString* replacementText = [NSMutableString stringWithString:@"#"];
        if (characterAtOffset == '#')
        {
            [replacementText setString:@""];
            shiftValue =  -1;
            insertLength = 1;
        }
        else
        {
            shiftValue = 1;
        }
        
        
        [codeView insertText:replacementText replacementRange:NSMakeRange(lineOffset, insertLength)];
        shift+=shiftValue;
    }
    [lineStarts  release];
}




#pragma mark == Full Screen ==

- (NSApplicationPresentationOptions)window:(NSWindow *)window willUseFullScreenPresentationOptions:(NSApplicationPresentationOptions)proposedOptions
{
    return (proposedOptions | NSApplicationPresentationAutoHideToolbar);
}



@end
